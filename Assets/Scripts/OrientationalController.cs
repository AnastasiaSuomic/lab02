﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class OrientationalController : MonoBehaviour
{
    [SerializeField] private GameObject horizontalLayout;
    [SerializeField] private GameObject verticalLayout;
    private ScreenOrientation _previousDeviceOrientation;
    
    // Start is called before the first frame update
    private void Start()
    {
        ChangeOrientationIfNeed(true);
    }

    private void ChangeOrientationIfNeed(bool forceChange)
    {
        var deviceOrientation = Screen.orientation;
        if (!forceChange && deviceOrientation == _previousDeviceOrientation)
        {
            return;
        }
        var isPortrait = deviceOrientation == ScreenOrientation.Portrait ||
                         deviceOrientation == ScreenOrientation.PortraitUpsideDown;
        if (isPortrait) 
        {
            horizontalLayout.SetActive(false);
            verticalLayout.SetActive(true);
        }
        else
        {
            horizontalLayout.SetActive(true);
            verticalLayout.SetActive(false);
        }

        _previousDeviceOrientation = deviceOrientation;
    }
    
    private void Update()
    {
        ChangeOrientationIfNeed(false);
    }
}

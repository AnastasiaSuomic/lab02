﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

[RequireComponent(typeof(Text))] //чтобы на объекте точно был компонент с типом Text
public class LocalizableText : MonoBehaviour
{
    private Text _textComponent;
    [SerializeField] private string tag; 
    private void Start()
    {
        UpdateText();
        LocalizationController.instance.languageChanged += InstanceOnlanguageChanged;
    }

    private void UpdateText()
    {
        _textComponent.text = LocalizationController.instance.GetLabel(tag);
    }

    private void OnDestroy()
    {
        LocalizationController.instance.languageChanged -= InstanceOnlanguageChanged;
    }

    private void InstanceOnlanguageChanged(Language newLanguage)
    {
        UpdateText();
    }

    private void Awake()
    {
        _textComponent = GetComponent<Text>();
    }
}
